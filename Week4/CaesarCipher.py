def encrypt(message, shift):
    secret = ""
    for c in message:
        if c.islower():
            num = ord(c)
            num += shift
            if num > ord("z"):     # wrap if necessary
                num -= 26
            elif num < ord("a"):
                num += 26
            secret = secret + chr(num)
        elif c.isupper():
            num = ord(c)
            num += shift
            if num > ord("Z"):     # wrap if necessary
                num -= 26
            elif num < ord("A"):
                num += 26
            secret = secret + chr(num)
        else:
            # don't modify any non-letters in the message; just add them as-is
            secret = secret + c
    print(secret)
    return secret


def decrypt(message, shift):
    shift = 26 - shift
    secret = ""
    for c in message:
        if c.islower():
            num = ord(c)
            num += shift
            if num > ord("z"):     # wrap if necessary
                num -= 26
            elif num < ord("a"):
                num += 26
            secret = secret + chr(num)
        elif c.isupper():
            num = ord(c)
            num += shift
            if num > ord("Z"):     # wrap if necessary
                num -= 26
            elif num < ord("A"):
                num += 26
            secret = secret + chr(num)
        else:
            # don't modify any non-letters in the message; just add them as-is
            secret = secret + c
    print(secret)
    return secret

encrypt('Hello, World!', 4)
decrypt('Lipps, Asvph!', 4)