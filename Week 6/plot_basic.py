# Author : Somnath Rakshit
import numpy as np
import matplotlib.pyplot as plt


def graph(formula, x_range):
    x = np.array(x_range)
    y = eval(formula)
    plt.plot(x, y)
    plt.show()


x_start = -np.pi
x_end = np.pi
num_of_intervals = 50

# graph('3*x + 4', np.linspace(x_start, x_end, num_of_intervals))
graph('np.sin(x)', np.linspace(x_start, x_end, num_of_intervals))
