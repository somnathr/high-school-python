# Author : Somnath Rakshit

import sympy as sy
x, y = sy.symbols('x,y', real=True)
print(sy.solve(x - 1 < y - 8, x))

from fillplots import plot_regions
import matplotlib.pyplot as plt
plotter = plot_regions([
    [(lambda x: x ** 2 - 2,),  # x ^ 2 > 2 and
     (lambda x: x + 2,)],  # x + 5 > 3
])
plt.show()